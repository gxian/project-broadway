#include <SoftwareSerial.h>

#define SLIP_ESC     0xDB
#define SLIP_END     0xC0
#define SLIP_ESC_ESC 0xDD
#define SLIP_ESC_END 0xDC

SoftwareSerial BTSerial(10, 11); // RX | TX

void setup()
{
  pinMode(9, OUTPUT);  // this pin will pull the HC-05 pin 34 (key pin) HIGH to switch module to AT mode
  digitalWrite(9, HIGH);
  Serial.begin(9600);
  //Serial.println("Enter AT commands:");
  BTSerial.begin(9600);  // HC-05 default speed in AT command more
}

boolean inFrame = false;

void loop()
{ 
  int8_t incoming;
  // Keep reading from HC-05 and send to Arduino Serial Monitor
  if (BTSerial.available()) 
  {
    incoming = BTSerial.read();
    switch(incoming)
    {
      case(SLIP_ESC):
        incoming = BTSerial.read();
        switch(incoming)
        {
          case(SLIP_ESC_END):
            Serial.print("\\");
            Serial.print(SLIP_END);
            Serial.print(" ");
            break;
          case(SLIP_ESC_ESC):
            Serial.print("\\");
            Serial.print(SLIP_ESC);
            Serial.print(" ");
            break;
          default:
            break;
        }
        break;
      case(SLIP_END):
        if (inFrame)
        {
          Serial.print("EOF");
          Serial.print("\n");
          inFrame = false;
        }
        else
        {
          inFrame = true;
          Serial.print("BOF ");
        }  
        break;
      default:
        Serial.print(incoming);
        Serial.print(" ");
    }
  }

  // Keep reading from Arduino Serial Monitor and send to HC-05
  if (Serial.available()) 
  {
    BTSerial.write(Serial.read());
  }
}
