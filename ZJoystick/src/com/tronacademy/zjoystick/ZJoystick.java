/****************************************************************************************
 * ZJoystick
 * Project: ZJoystick
 * Author: George Xian
 * Licensor: (C) Tron Academy
 * Purpose: A self-contained Android View element representing a joystick
 * 
 * Disclaimer: 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE 
 * OR OTHER DEALINGS IN THE SOFTWARE.
 ***************************************************************************************/

package com.tronacademy.zjoystick;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

/**
 * ZJoystick class handles the graphics, user-interface and provides an API
 * to customize and access its status the ZJoystick widget. 
 * The ZJoystick widget simulates a joystick.  
 * 
 * @author George Xian
 * @version 0.1 
 *
 */
public class ZJoystick extends View {
	
	private static final String TAG = "ZJoystick::ZJoystick";
	
	/* ********************* Class variables *********************** */ 
	//processor objects
	private OnJoystickMoveListener mOnJoystickMoveListener; 
	
	//public setting flags
	private boolean allowTouch = true;
	private boolean snapToCenterX = true;
	private boolean snapToCenterY = true;
	private boolean squareMode = false;
	private boolean pollMode = false;
	
	//touch status
	private int centerXPos;					//should only be changed by onMeasure()
	private int centerYPos;					//should only be changed by onMeasure()
	private int stickXPos;				
	private int stickYPos;				
	private int lastViewStickXPos;			//last x position value view has been updated to
	private int lastViewStickYPos;			//last y position value view has been updated to
	private final int ACTION_PROGRAM_DRIVE = -1 ;	//constant to represent program driving stick movement
	
	//size parameters
	private final float MIN_ZERO_RANGE = (float) 0.0001;
	public int TOUCH_TOLERANCE = 1;			//touch acceptance radius of the stick relative to it's graphical radius
	private float TRAVEL_RAD_PERCENT = (float) 0.7;//percentage of maxRad travelRad should be
	private int PADDING = 10;				//how much extra room the widget takes up
	private int maxRad;						//should only be changed by onMeasure()
	private int travelRad; 					//maximum radius of stick travel
	private int stickRad;  					//should only be changed by onMeasure()
	private boolean hasCentered = false;	//has joystick stick graphic been placed in center
	private boolean wasTracking = false;	//was the last touch event a tracking event
	
	//handlers for graphics
	private Bitmap mJoystick_stick;
	private Bitmap mJoystick_bg;
	Paint mJoystick_stick_paint = new Paint();
	Paint mJoystick_bg_paint = new Paint();
	
	//update view handler
	private boolean touchProcessing = false;
	private boolean graphicUpdating = false;
	private Handler updateViewHandler = new Handler();
	
	/* ********************* Public methods *********************** */
	/**
	 * 
	 * @author Ultima1
	 *
	 */
	public interface OnJoystickMoveListener {
		/**
		 * Returns the current position of the stick upon a touch event.  
		 * @param x		value from -128->127 representing x position
		 * @param y 	value from -128->127 representing y position
		 */
		public void onValueChanged(int x, int y);
		
		/**
		 * Action to perform while joystick is tracking gesture
		 */
		public void onTracking();
		
		/**
		 * Action to perform when joystick stops tracking gesture
		 */
		public void onStopTracking();
		
		/**
		 * Action to perform when stick reaches a boundary value
		 */
		public void onHitBoundary();
	}
	
	/**
	 * Constructor for the ZJoystick instance
	 * @param context	current Context
	 */
	public ZJoystick(Context context, AttributeSet attributeSet) {
		super(context, attributeSet);
		loadDefaultGraphics(context.getResources());
	}
	
	/**
	 * Set properties of the joystick tracking process
	 * @param listener			listener object to use
	 */
	public void setOnJoystickMoveListener(OnJoystickMoveListener listener) {
		this.mOnJoystickMoveListener = listener;
	}
	
	/**
	 * Set whether touch events should be enabled for disabled
	 * @param setTouch		boolean with value 'true' to enable 'false' for disable
	 */
	public void setAllowTouch(boolean setTouch) {
		allowTouch = setTouch;
	}
	
	/**
	 * Set update mode to be callback driven or externally polled
	 * @param isPoll		set false to use callback driven, set true to poll externally  
	 */
	public void setPollMode(boolean isPoll) {
		pollMode = true;		
	}
	
	/**
	 * Set the stick position
	 * @param x		new x position of stick 
	 * @param y		new y position of stick in square mode
	 */
	public void setPos(int x, int y) {
		/* Program driven events must follow the rules of normal touch events  
		 * such as boundaries. However it does not return to center because an 
		 * 'ACTION_UP' event doesn't get called */
		
		new ProcessTouchTask().execute(ACTION_PROGRAM_DRIVE, x, y);
	}
	
	/**
	 * Gets the stick position
	 * @return		Array of the joystick position
	 */
	public int[] getPos() {
		int[] ret = {(int) ((float) (stickXPos - centerXPos)/travelRad*127), 
					 (int) ((float) (centerYPos - stickYPos)/travelRad*127)};
		return ret;
	}
	
	/**
	 * Set whether stick should return to x-axis center or stay at last x position
	 * when released
	 * @param setSnap		boolean with value 'true' for return and 'false' for stay 
	 */
	public void setSnapCenterX(boolean setSnap) {
		snapToCenterX = setSnap;		
	}
	
	
	/**
	 * Return stick has been set to return to x-axis center when released
	 * @return		boolean representing stick has been set to snap to x center
	 */
	public boolean getSnapCenterX() {
		return snapToCenterX;
	}
	
	/**
	 * Set whether stick should return to y-axis center or stay at last y position
	 * when released
	 * @param setSnap		boolean with value 'true' for return and 'false' for stay
	 */
	public void setSnapCenterY(boolean setSnap) {
		snapToCenterY = setSnap;		
	}
	
	/**
	 * Return stick has been set to return to y-axis center when released
	 * @return		boolean representing stick has been set to snap to y center
	 */
	public boolean getSnapCenterY() {
		return snapToCenterY;
	}
	
	/**
	 * Set whether joystick should operate in square mode, note square mode changes
	 * the graphic used
	 * @param setSquare		boolean with value 'true' for square mode and 'false' for normal
	 */
	public void setSquareMode(boolean setSquare) {
		squareMode = setSquare;
		loadDefaultGraphics(getContext().getResources());
		new ProcessTouchTask().execute(ACTION_PROGRAM_DRIVE, stickXPos, stickYPos);
		updateViewHandler.post(updateViewTask);
	}
	
	/* ********************* UI functions *********************** */
	
	/**
	 * Prepare graphics for ZJoystick view
	 * @param res	resource tag
	 */
	private void loadDefaultGraphics(Resources res) {
		mJoystick_stick = (Bitmap) BitmapFactory.decodeResource(res, R.drawable.joystick_stick);
		if (squareMode) {
			mJoystick_bg = (Bitmap) BitmapFactory.decodeResource(res, R.drawable.joystick_bg_square);
		} else {
			mJoystick_bg = (Bitmap) BitmapFactory.decodeResource(res, R.drawable.joystick_bg);
		}
	}
	
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {		
		//get the center of view
		centerXPos = getWidth() / 2;
		centerYPos = getWidth() / 2;
		
		//get size of bitmap elements
		stickRad = mJoystick_stick.getHeight() / 2;
		maxRad = mJoystick_bg.getHeight() / 2;
		travelRad = (int) (TRAVEL_RAD_PERCENT * maxRad);
		
		//consume as much room as the background plus allow for overhang by the stick's radius
		setMeasuredDimension((int) (2*maxRad + 2*stickRad + PADDING), (int) (2*maxRad + 2*stickRad + PADDING));
	}
	
	/**
	 * Returns the joystick stick graphic
	 * @return	Bitmap of joystick stick graphic
	 */
	public Bitmap get_joystick_stick() {
		return mJoystick_stick;
	}
	
	/**
	 * Returns the joystick background graphic
	 * @return	Bitmap of joystick background graphic
	 */
	public Bitmap get_joystick_bg() {
		return mJoystick_bg;
	}
	
	/**
	 * Sets the graphic to use for the joystick stick
	 * @param joystick	Bitmap to use as joystick stick
	 */
	public void set_joystick_stick(Bitmap joystick) {
		mJoystick_stick = joystick;
	}
	
	/**
	 * Sets the graphic to use for the joystick background
	 * @param joystickBG	Bitmap to use as joystick background
	 */
	public void set_joystick_bg(Bitmap joystickBG) {
		mJoystick_bg = joystickBG;
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		//first draw must center the stick graphic
		if (!hasCentered) {
			stickXPos = centerXPos;
			stickYPos = centerYPos;
			hasCentered = true;
		}
		
		float bgDrawX = (float) (centerXPos - maxRad);
		float bgDrawY = (float) (centerYPos - maxRad);
		
		float stickDrawX = (float) (stickXPos - stickRad);
		float stickDrawY = (float) (stickYPos - stickRad);
		
		canvas.drawBitmap(mJoystick_bg, bgDrawX, bgDrawY, mJoystick_bg_paint);
		canvas.drawBitmap(mJoystick_stick, stickDrawX, stickDrawY, mJoystick_bg_paint);
	}
	
	public boolean onTouchEvent(MotionEvent event) {
		if (allowTouch) {
			final int action = event.getAction();
			final int touchXPos = (int) event.getX();
			final int touchYPos = (int) event.getY();
			
			//block further touch processing tasks unless a release event occurs
			if (!touchProcessing || action == MotionEvent.ACTION_UP) {
				new ProcessTouchTask().execute(action, touchXPos, touchYPos);
				touchProcessing = true;		
			}
			
			return true;
		} else {
			return false;
		}
	}
	
	
	/* ********************* Multi-threading parameters *********************** */

	private Runnable updateViewTask = new Runnable () {
		@Override
		public void run() {
			/* no need to use synchronized methods for lastViewStickXPos variables 
			 * because this Runnable is called by Handler which is thread safe
			 * (technically don't need to call 'getCurrentStickXPos()' or 'getCurrentStickYPos()'
			 * either but just keep the convention.
			 * */ 
			
			lastViewStickXPos = stickXPos;
			lastViewStickYPos = stickYPos;
			
			invalidate();
			graphicUpdating = false;	//unblock further update tasks from scheduling
		}
	};
	
	/**
	 * The ProcessTouchTask class is used to schedule a touch event computation task 
	 * on a separate thread.
	 * 
	 * @author Ultima1
	 *
	 */
	class ProcessTouchTask extends AsyncTask<Integer, Integer, Void> {
		
		private boolean hitBoundary = false;
		
		@Override
		protected Void doInBackground(Integer... params) {
			final int action = params[0];
			final int touchXPos = params[1];
			final int touchYPos = params[2];
			
			//cache for thread safety
			final int currentStickXPos = stickXPos;
			final int currentStickYPos = stickYPos;
			
			final int dx = touchXPos - centerXPos;
			final int dy = centerYPos - touchYPos;	//co-ordinates of OS is 0 at the top, want 0 at bottom
			
			if (action == MotionEvent.ACTION_DOWN || action == MotionEvent.ACTION_MOVE || action == ACTION_PROGRAM_DRIVE) {
				boolean trackGesture = false;
				//only process if stick is touched or it was already tracking touch
				boolean touchedStick = ((touchXPos - currentStickXPos)*(touchXPos - currentStickXPos) + (touchYPos - currentStickYPos)*(touchYPos - currentStickYPos)) <= TOUCH_TOLERANCE*stickRad*stickRad;
				if (touchedStick || wasTracking || action == ACTION_PROGRAM_DRIVE) {
					trackGesture = true;
				}
				
				if (trackGesture) {
					if (squareMode) {
						if (dx > travelRad) {
							//hit x right boundary
							stickXPos = centerXPos + travelRad;
							hitBoundary = true;
						} else if (dx < -travelRad) {
							//hit x left boundary
							stickXPos = centerXPos - travelRad;
							hitBoundary = true;
						} else {
							//within bounds
							stickXPos = touchXPos;
						}
						
						if (dy > travelRad) {
							//hit lower y boundary
							stickYPos = centerYPos-travelRad;
							hitBoundary = true;
						} else if (dy < -travelRad) {
							//hit upper y boundary
							stickYPos = centerYPos + travelRad;
							hitBoundary = true;
						} else {
							//within boundary
							stickYPos = touchYPos;
						}
				
						
					} else {
						int appliedRad = (int) Math.sqrt(dx*dx + dy*dy);
						if (appliedRad <= travelRad) {
							//track gesture
							stickXPos = touchXPos;
							stickYPos = touchYPos;
						} else {
							//hit boundary case
							stickXPos = dx * travelRad / appliedRad + centerXPos;
							stickYPos = centerYPos - dy * travelRad / appliedRad;
							hitBoundary = true;
						}
					}
					wasTracking = true;
				} else {
					//didn't touch stick so snap back to center if snap is enabled
					if (snapToCenterX) {
						stickXPos = centerXPos;
					}
					if (snapToCenterY) {
						stickYPos = centerYPos;
					}
					wasTracking = false;				
				}						
			} else if (action == MotionEvent.ACTION_UP) {
				//stop tracking gesture and snap stick back to center if snap is enabled
				if (snapToCenterX) {
					stickXPos = centerXPos;
				}
				if (snapToCenterY) {
					stickYPos = centerYPos;
				}
				wasTracking = false;	
			}	
			
			return null;
		}
		
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			
			//output status
			int sdx = stickXPos - centerXPos;
			int sdy = centerYPos - stickYPos;	
			
			if (!pollMode) {
				mOnJoystickMoveListener.onValueChanged((int) ((float) sdx/travelRad*127), (int) ((float) sdy/travelRad*127));
			}
			
			touchProcessing = false;	//unblock scheduling of touch processing tasks 
			
			if (wasTracking) {
				//return tracking status
				if (!pollMode) {
					mOnJoystickMoveListener.onTracking();
				}
				
				//schedule a view update task
				if (!graphicUpdating) {
					updateViewHandler.post(updateViewTask);
					graphicUpdating = true;		//block further update tasks from scheduling
				}
			} else {
				//return stopped tracking status
				if (!pollMode) {
					mOnJoystickMoveListener.onStopTracking();
				}
				
				//if last view update did not return stick to center position, schedule another view update
				boolean stickXPosIsZero = (lastViewStickXPos < (centerXPos+MIN_ZERO_RANGE)) && (lastViewStickXPos > (centerXPos-MIN_ZERO_RANGE));
				boolean stickYPosIsZero = (lastViewStickYPos < (centerYPos+MIN_ZERO_RANGE)) && (lastViewStickYPos > (centerYPos-MIN_ZERO_RANGE));
				if (!stickXPosIsZero || !stickYPosIsZero) {
					updateViewHandler.post(updateViewTask);
				}
			}
			
			if (hitBoundary && !pollMode) {
				mOnJoystickMoveListener.onHitBoundary();
			}
		}
	}
}

