package com.tronacademy.phantom.Utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Arrays;

import android.util.Log;

/**
 * Framing protocol built on top of SLIP. Frame format is:
 * <p>
 * <li>1 start byte (defined by SLIP as 0xC0)
 * <li>4 bytes ID
 * <li>8 bytes payload
 * <li>1 stop byte  (defined by SLIP as 0xC0)
 * <p>
 * 
 *  <p>
 *  ESC character defined by SLIP as 0xDB, prefixes bytes to 
 *  allow 0xC0 and 0xDB characters to appear inside the frame
 *  but get replaced by 0xDC and 0xDD respectively
 *  <p>
 *  
 *  <p>
 *  This class manages the frame structure and wrapping for 
 *  sending
 *  <p> 
 * 
 * @author George Xian
 *
 */
public class KillalotFrame {
	public final static int ID_SIZE = 4;
	public final static int DATA_SIZE = 8;
	
	public final static byte SLIP_END = (byte) 0xC0;
	public final static byte SLIP_ESC = (byte) 0xDB;
	public final static byte SLIP_ESC_END = (byte) 0xDC;
	public final static byte SLIP_ESC_ESC = (byte) 0xDD;
	
	private boolean byteStreamUpdated = false;
	private ByteArrayOutputStream byteStreamForm = new ByteArrayOutputStream(26);
	private int streamLen = 0; 
	
	private byte[] ID = new byte[ID_SIZE];
	private byte[] data = new byte[DATA_SIZE];
	
	
	/**
	 * Sets the ID field of this frame
	 * 
	 * @param newID		array of 4 bytes, anything after that will be ignored
	 */
	public void setID(byte[] newID) {
		if (newID.length > 3) {
			ID = Arrays.copyOfRange(newID, 0, 4);
			byteStreamUpdated = false;
		}
	}
	
	/**
	 * Sets the payload data of this frame
	 * 
	 * @param newData	array of 8 bytes, anything after that will be ignored
	 */
	public void setData(byte[] newData) {
		if (newData.length > 7) {
			data = Arrays.copyOfRange(newData, 0, 8);
			byteStreamUpdated = false;
		}
	}
	
	/**
	 * Get the set ID field
	 * 
	 * @return ID field of this frame with escape characters
	 */
	public byte[] getID() {
		return ID;
	}
	
	/**
	 * Get the set data field
	 * 
	 * @return data field of this frame with escape characters
	 */
	public byte[] getData() {
		return data;
	}
	
	/**
	 * Add necessary escape characters and convert to byte stream
	 * 
	 * @return byte array with escape characters added 
	 */
	public ByteArrayOutputStream byteStreamForm() {
		if (!byteStreamUpdated) {
			// must re-calculate stream
			/* reset byte stream */
			try {
				byteStreamForm.flush();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			streamLen = 0;			
			
			/* write begin character */
			byteStreamForm.write(SLIP_END);
			streamLen++;
			
			/* write ID */
			for (int i=0; i<ID_SIZE; i++) {
				switch(ID[i]) {
				case(SLIP_END):
					byteStreamForm.write(SLIP_ESC);
					byteStreamForm.write(SLIP_ESC_END);
					streamLen += 2;
					break;
				case(SLIP_ESC):
					byteStreamForm.write(SLIP_ESC);
					byteStreamForm.write(SLIP_ESC_ESC);
					streamLen += 2;
					break;
				default:
					byteStreamForm.write(ID[i]);
					streamLen++;
				} 
			}
			
			/* write data */
			for (int i=0; i<DATA_SIZE; i++) {
				switch(data[i]) {
				case(SLIP_END):
					byteStreamForm.write(SLIP_ESC);
					byteStreamForm.write(SLIP_ESC_END);
					streamLen += 2;
					break;
				case(SLIP_ESC):
					byteStreamForm.write(SLIP_ESC);
					byteStreamForm.write(SLIP_ESC_ESC);
					streamLen += 2;
					break;
				default:
					byteStreamForm.write(data[i]);
					streamLen++;
				}
			}
			
			/* write end character */
			byteStreamForm.write(SLIP_END);
			streamLen++;
			byteStreamUpdated = true;
			
			return byteStreamForm;
			
		} else {
			// return the cached form
			return byteStreamForm;
		}
	}
	
	/**
	 * Returns byte stream in numerical string form for 
	 * verification purposes
	 * 
	 * @return string of the byte stream
	 */
	public String stringForm() {
		if (!byteStreamUpdated) {
			// update before converting to string form
			byteStreamForm = byteStreamForm();			
		} 
		byte[] byteArray = byteStreamForm.toByteArray();
		
		StringBuilder strBuilder = new StringBuilder();
		for (int i=0; i<streamLen; i++) {
			strBuilder.append(String.format("%d ", byteArray[i]));
		}
		return strBuilder.toString();
	}
}