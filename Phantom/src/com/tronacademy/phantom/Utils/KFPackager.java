package com.tronacademy.phantom.Utils;

import java.io.IOException;
import java.util.Arrays;

import android.util.Log;

/**
 * <p>
 * Component which packages OutputMixer type outputs into 
 * Killalot frames.
 * <p>
 * 
 * <p>
 * This component is designed to be attached to a communication
 * manager to send frames over the chosen communication method
 * <p>
 * 
 * @author George Xian
 *
 */
public class KFPackager {
	
	private static final String TAG = "Phantom::KFPackager";
	
	private BtCommManager commMgmr = null; 
	
	/**
	 * Packages data array into Killalot frames then sends them
	 * if a valid communication manager is attached
	 * 
	 * @param data		
	 */
	public void sendData(byte[] data, int bytes) {
		
		/* allocates frames for this transmission */
		int numberOfFrames = (int) Math.ceil((double) bytes / 8);
		KillalotFrame[] frames = new KillalotFrame[numberOfFrames]; 
		
		/* packaging data into multiple frames */
		for (int frame=0; frame<numberOfFrames; frame++) {
			byte[] frameID = {1, 0, 0, (byte) (KillalotFrame.DATA_SIZE*frame)};
			
			int beginIndex = KillalotFrame.DATA_SIZE*frame;
			int endIndex = KillalotFrame.DATA_SIZE*(frame+1);
			
			if (endIndex > bytes) {
				endIndex = bytes;
			}
			
			frames[frame] = new KillalotFrame();
			frames[frame].setID(frameID);
			frames[frame].setData(Arrays.copyOfRange(data, beginIndex, endIndex));
		}
		
		/* send data */
		if (commMgmr != null) {
			for (int frame=0; frame<numberOfFrames; frame++) {
				commMgmr.send(frames[frame].byteStreamForm());
				//Log.d(TAG, String.format("Streamed: %s", frames[frame].stringForm()));
			}
		} else {
			Log.e(TAG, "No communication manager attached");
		}
	}
	
	/**
	 * 
	 * @param mgmr
	 */
	public void bindCommManager(BtCommManager mgmr) {
		commMgmr = mgmr;
	}
}
