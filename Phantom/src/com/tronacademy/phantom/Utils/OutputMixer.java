package com.tronacademy.phantom.Utils;

import android.app.Activity;
import android.os.Handler;
import android.util.Log;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;

import com.tronacademy.phantom.R;
import com.tronacademy.zjoystick.ZJoystick;
import org.ejml.simple.*;

/**
 * <p>
 * Component which manages input controls and mixes their
 * outputs.
 * <p>
 * 
 *  <p>
 *  This component is designed to be attached to a FramePackager
 *  component to wrap the output signals into a communication 
 *  protocol frame
 *  <p> 
 * 
 * @author George Xian
 *
 */
public class OutputMixer {
	
	private static final String TAG = "Phantom::OutputMixer";
	
	private Activity parentActivity;
	private KFPackager framePackager = null;
	
	private ZJoystick joystickL;
	private ZJoystick joystickR;
	private SeekBar hoverPitch;
	private SeekBar hoverThrottle;
	
	private final int NUM_CHANS = 16;
	private final int ANALOG_CHANS = 6;
	private SimpleMatrix chanMixer = SimpleMatrix.identity(ANALOG_CHANS);
	private SimpleMatrix analogChanIn = new SimpleMatrix(ANALOG_CHANS, 1);
	private SimpleMatrix analogChanOut = new SimpleMatrix(ANALOG_CHANS, 1);
	
	private int sendInterval = 30;
	private Thread mixAndSendThread;
	private Runnable mixAndSendTask;
	private Handler mixAndSendHandler = new Handler();
	
	/**
	 * Set the Joystick view objects to bind output
	 * 
	 * @param l			left joystick view
	 * @param r			right joystick view
	 */
	public void layoutControls(Activity activity) {
		parentActivity = activity;
		
		initJoysticks();
		initSeekbars();
	}
	
	/**
	 * Create a new thread which processes inputs and broadcasts
	 * values out via bluetooth
	 */
	public void startOutput() {
		mixAndSendTask = new Runnable() {

			@Override
			public void run() {
				analogChanIn.set(0, joystickL.getPos()[0]);
				analogChanIn.set(1, joystickL.getPos()[1]);
				analogChanIn.set(2, joystickR.getPos()[0]);
				analogChanIn.set(3, joystickR.getPos()[1]);
				
				//Log.d(TAG, String.format("%.2f, %.2f, %.2f, %.2f", analogChanIn.get(0), analogChanIn.get(1), analogChanIn.get(2), analogChanIn.get(3)));
				
				analogChanOut = chanMixer.mult(analogChanIn);
				
				/* extract data into byte form */
				byte[] chanOutByteArray = new byte[NUM_CHANS];
				for (int i=0; i<ANALOG_CHANS; i++) {	// ANALOG CHANS -128->127
					chanOutByteArray[i] = (byte) analogChanOut.get(i);
				}
				for (int i=ANALOG_CHANS; i<NUM_CHANS; i++) {
					chanOutByteArray[i] = 0;
				}
				
				if (framePackager != null) {			// DIGITAL CHANS 0->127
					framePackager.sendData(chanOutByteArray, NUM_CHANS);					
				} else {
					Log.e(TAG, "No frame packager attached");
				}
			}
		};
		
		mixAndSendThread = new Thread() {
			@Override
			public void run() {
				try {
					while(true) {
						sleep(sendInterval);
						mixAndSendHandler.post(mixAndSendTask);
					}
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		};
		
		mixAndSendThread.start();
	}
	
	/**
	 * Stops the thread which outputs bluetooth data
	 */
	public void stopOutput() {
		mixAndSendThread.interrupt();
		mixAndSendThread = null;
	}
	
	/**
	 * Choose what frame packager should be used to wrap outputs 
	 */
	public void bindFramePackager(KFPackager packager) {
		framePackager = packager;
	}
	
	/**
	 * Set the interval between bluetooth sends
	 */
	public void setSendInterval(int interval) {
		if (interval > 0) {
			sendInterval = interval; 
		}
	}
	
	private void initJoysticks() {
		joystickL = (ZJoystick) parentActivity.findViewById(R.id.joystickL);
		joystickL.setPollMode(true);
		joystickR = (ZJoystick) parentActivity.findViewById(R.id.joystickR);
		joystickR.setPollMode(true);
		Log.d(TAG, "Instantiated joysticks");
	}
	
	private void initSeekbars() {
		hoverPitch = (SeekBar) parentActivity.findViewById(R.id.seekHoverPitch);
		hoverThrottle = (SeekBar) parentActivity.findViewById(R.id.seekHoverThrottle);
		
		hoverPitch.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				analogChanIn.set(4, progress - 127);
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
				
			}			
		});
		
		hoverThrottle.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				analogChanIn.set(5, progress - 127);
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
				
			}
			
		});	
		
		Log.d(TAG, "Instantiated seekbars");
	}
		
}