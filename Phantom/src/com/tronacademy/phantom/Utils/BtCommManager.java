package com.tronacademy.phantom.Utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.util.Log;

/**
 * 
 * @author Ultima1
 *
 */
public class BtCommManager {
	private final static String TAG = "Phantom::BtCommManager";
	
	private final static int REQUEST_ENABLE_BT = 1;
	private final static UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
	
	private AcceptThread mAcceptThread;
	private BluetoothSocket mBluetoothSocket = null;
	private String connectedDevName;
	
	private class AcceptThread extends Thread {
		private final BluetoothSocket s;
		private final BluetoothDevice d;
		
		public AcceptThread(BluetoothDevice device) {
			BluetoothSocket tmp = null;		// used before assigning to socket because final
			try {
				// MY_UUID is the app's UUID string, also used by the client code
				tmp = device.createInsecureRfcommSocketToServiceRecord(MY_UUID);
			} catch (IOException e) {
				Log.e(TAG, "Failed to obtain socket");
			}
			s = tmp;
			d = device;
		}
		
		public void run() {
			/* 
			 * Because BluetoothAdapter.discovery() is a heavy weight procedure,
			 * cancel any on going discovery before attempting connect
			 */
			BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
			btAdapter.cancelDiscovery();
			
			/*
			 * Connect the device through the socket. This is a block call so it 
			 * will return on a successful connection or exception
			 */
			try {
				s.connect();
				mBluetoothSocket = s;
			} catch (IOException e1) {
				try {
					/* Couldn't connect, close socket and return */
					s.close();
				} catch (IOException e2) {
					//TODO Auto-generated catch block
					e2.printStackTrace();
				}
				mBluetoothSocket = null;
			}
			connectedDevName = d.getName();
			
			/* reset AcceptThread because we are done */
			synchronized (BtCommManager.this) {
				mAcceptThread = null;
			}
		}
	}
	
	/**
	 * Starts an intent to turn on bluetooth on device 
	 * @return flag representing whether device has bluetooth support	
	 */
	public boolean startBluetooth(Activity activity) {
		BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
		if (btAdapter == null) {
			return false;
		}
		
		if (!btAdapter.isEnabled()) {
			Intent enableBtIntent = new Intent (BluetoothAdapter.ACTION_REQUEST_ENABLE);
			activity.startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
		}
		return true;
	}
	
	/**
	 * Return a list of paried devices
	 * @return list of paired devices
	 */
	public List<BluetoothDevice> getPairedDevices() {
		BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
		Set<BluetoothDevice> pairedDevices = btAdapter.getBondedDevices();
		
		List<BluetoothDevice> devices = new ArrayList<BluetoothDevice>();
		devices.addAll(pairedDevices);
		
		return devices;
	}
	
	/**
	 * Start connection with chosen device
	 * @param device	Device to connect with
	 */
	public synchronized void connect(BluetoothDevice device) {
		mAcceptThread = new AcceptThread(device);
		mAcceptThread.start();
	}
	
	/**
	 * Disconnect with currently connected device
	 */
	public void disconnect() {
		try {
			mBluetoothSocket.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Call after calling the connect() method to determine whether 
	 * connection thread is finished 
	 * @return flag representing whether connection was finished
	 */
	public boolean isConnectFinished() {
		return (mAcceptThread == null) ? true : false; 
	}
	
	/**
	 * Call after isConnectFinished() method to determine whether
	 * a connection was successful
	 * @return flag representing whether connection was successful
	 */
	public boolean isConnectSuccess() {
		return mBluetoothSocket.isConnected();
	}
	
	/**
	 * Returns the name of the connected device
	 * @return string of the connected device's name
	 */
	public String getConnectedDevName() {
		return connectedDevName;
	}
	
	/**
	 * Send message to connected device
	 * @param message	Byte stream of message to send
	 */
	public void send(ByteArrayOutputStream data) {
		if (BluetoothAdapter.getDefaultAdapter().isEnabled() && mBluetoothSocket != null) {
			OutputStream toSend = null;
			try {
				toSend = mBluetoothSocket.getOutputStream();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				Log.e(TAG, "Failed to get output stream");
				e1.printStackTrace();
			}
			
			try {
				data.writeTo(toSend);
				toSend.flush();
				Log.d(TAG, String.format("Sent: %s", numStringForm(data)));
			} catch (IOException e2) {
				// TODO Auto-generated catch block
				Log.e(TAG, "Failed to write to output stream");
				e2.printStackTrace();
			}
		} else {
			Log.e(TAG, "Bluetooth disabled or invalid socket");
		}
	}
	
	private String numStringForm(ByteArrayOutputStream stream) {
		byte[] byteArray = stream.toByteArray();
		
		StringBuilder strBuilder = new StringBuilder();
		for (int i=0; i<byteArray.length; i++) {
			strBuilder.append(String.format("%d ", byteArray[i]));
		}
		return strBuilder.toString();		
	}
}
