package com.tronacademy.phantom.Activities;

import com.tronacademy.phantom.Phantom;
import com.tronacademy.phantom.R;
import com.tronacademy.phantom.Utils.BtCommManager;
import com.tronacademy.phantom.Utils.KFPackager;
import com.tronacademy.phantom.Utils.OutputMixer;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ToggleButton;

public class ControlPad extends Activity {
	
	private static final String TAG = "Phantom::ControlPad";
	
	private ToggleButton sendToggle;
	
	private static OutputMixer mixer = new OutputMixer();
	private static KFPackager packager = new KFPackager();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_control_pad);
		
		/* connect pipelines */
		packager.bindCommManager(((Phantom) getApplicationContext()).getBtManager());
		mixer.bindFramePackager(packager);
		
		/* start sending */
		mixer.layoutControls(this);
		
		/* button to enable sending */
		sendToggle = (ToggleButton) findViewById(R.id.output_start);
		sendToggle.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked) {
					mixer.startOutput();					
				} else {
					mixer.stopOutput();					
				}
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.control_pad, menu);
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		switch (item.getItemId()) {
		case R.id.devices:
			pairDevices();
			return true;
		case R.id.mix:
			mixOutputs();
			return true;
		case R.id.action_settings:
			configSettings();	
			
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
	private void pairDevices() {
		sendToggle.setChecked(false);	// turn off sending activity before configuring bluetooth
		Log.d(TAG, "Starting BtPair activity");
		Intent intent = new Intent(this, BtPairMenu.class);
		startActivity(intent);
	}
	
	private void mixOutputs() {
		Log.d(TAG, "Starting OutputMix activity");
	}
	
	private void configSettings() {
		Log.d(TAG, "Selected settings menu options");
	}
}
