package com.tronacademy.phantom.Activities;

import java.util.ArrayList;
import java.util.List;

import com.tronacademy.phantom.Phantom;
import com.tronacademy.phantom.R;
import com.tronacademy.phantom.Utils.BtCommManager;

import android.app.ListActivity;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

/**
 * 
 * @author George Xian
 *
 */
public class BtPairMenu extends ListActivity {
	private static final String TAG = "Phantom::BtPairMenu";
	private final int RESCAN_INT = 1000;
	private final int CONN_POLL_INT = 100;
	
	static BtCommManager btConnector;
	
	private List<BluetoothDevice> pairedDevices;
	private Handler refreshPairedDevicesHandler = new Handler();
	private ProgressBar spinner; 
	
	private Runnable refreshPairedDevicesTask = new Runnable() {
		@Override
		public void run() {
			populatePairedDeviceMenu();
		}
	};
	
	private Handler connectionPollHandler = new Handler();
	private Thread refreshPairedDevicesThread = new Thread() {
		@Override
		public void run() {
			try {
				while(true) {
					refreshPairedDevicesHandler.post(refreshPairedDevicesTask);
					sleep(RESCAN_INT);
				}
			} catch (InterruptedException e) {
				//TODO Auto-generated catch block
				Log.d(TAG, "No longer checking connection status");				
			}
		}
	};
	
	private Thread checkConnectionStatusThread = null; 
	private Runnable checkConnectionStatusTask = new Runnable() {
		@Override
		public void run() {
			if (btConnector.isConnectFinished()) {
				runOnUiThread(new Runnable () {
					@Override
					public void run() {
						spinner.setVisibility(View.GONE);
						
						/* Tell user whether connection was success failure */
						if (btConnector.isConnectSuccess()) {
							String successMsg = getResources().getString(R.string.bt_connection_success); 
							Toast.makeText(getApplicationContext(), String.format(successMsg, btConnector.getConnectedDevName()), Toast.LENGTH_SHORT).show();
						} else {
							String failMsg = getResources().getString(R.string.bt_connection_failure);
							Toast.makeText(getApplicationContext(), String.format(failMsg, btConnector.getConnectedDevName()), Toast.LENGTH_SHORT).show();
						}
					}
				});
				checkConnectionStatusThread.interrupt();
				checkConnectionStatusThread = null;		// disable checking thread
			}
		}
	};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_btpair);
		
		btConnector = ((Phantom) getApplicationContext()).getBtManager(); 
		
		/* raise Toast if Bluetooth not supported on device */
		if(!btConnector.startBluetooth(this)) {
			Toast.makeText(getApplicationContext(), R.string.no_bt_support, Toast.LENGTH_SHORT).show();
		}
		
		spinner = (ProgressBar) findViewById(R.id.connection_progress);
		spinner.setVisibility(View.GONE);
		
		refreshPairedDevicesThread.start();		// periodically refresh paried devices list
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.btpair, menu);
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		switch (item.getItemId()) {
		case R.id.cpad:
			backToControlPad();
			return true;
		case R.id.btdisconnect:
			disconnectDevice();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
	@Override
	protected void onListItemClick(ListView l, View v, int pos, long id) {
		super.onListItemClick(l, v, pos, id);
		
		spinner.setVisibility(View.VISIBLE);
		spinner.setIndeterminate(true);
		
		BluetoothDevice selectedDevice = pairedDevices.get(pos);
		String connectInitMsg = getResources().getString(R.string.bt_connection_init); 
		Toast.makeText(getApplicationContext(), String.format(connectInitMsg, selectedDevice.getName()), Toast.LENGTH_SHORT).show();
		btConnector.connect(selectedDevice);
		
		/* poll connection status, give UI feedback */
		checkConnectionStatusThread = new Thread() {
			@Override
			public void run() {
				try {
					while(true) {
						sleep(CONN_POLL_INT);
						connectionPollHandler.post(checkConnectionStatusTask);
					}
				} catch (InterruptedException e) {
					//TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		};
		checkConnectionStatusThread.start();
	}
	
	private void backToControlPad() {
		Log.d(TAG, "Returning to ControlPad activity");
		Intent intent = new Intent(this, ControlPad.class);
		startActivity(intent);
	}
	
	private void disconnectDevice() {
		String devName = btConnector.getConnectedDevName();
		btConnector.disconnect();
		
		String connectInitMsg = getResources().getString(R.string.bt_connection_closed); 
		Toast.makeText(getApplicationContext(), String.format(connectInitMsg, devName), Toast.LENGTH_SHORT).show();
	}
	
	private void populatePairedDeviceMenu() {
		pairedDevices = btConnector.getPairedDevices();		// create device list
		
		/* getting device names for ListView */
		List<String> s = new ArrayList<String>();
 		for (BluetoothDevice bt : pairedDevices) {
			s.add(bt.getName());
		}

		setListAdapter(new ArrayAdapter<String>(this, R.layout.bt_list, R.id.bttext1, s));
	}
}