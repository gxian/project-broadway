/**
 * Chicago - Project Broadway
 *
 * main.cpp
 * Purpose: Parent routine handler for basic bluetooth motor controller
 *
 * @author George Xian
 * @version 0.1	20-02-2014
 */

#include "main.hpp"
#include "config.hpp"

#include <Arduino.h>
#include <SoftwareSerial/SoftwareSerial.h>

#include "drvUtil/PWMWheel.hpp"
#include "drvUtil/OmniDrive4.hpp"


PWMWheel wheel[4];		//! wheel instances for drive system
OmniDrive4 drive;		//! drive system

SoftwareSerial bluetooth(10, 9);	//! bluetooth UART instance

/**
 * Initialization routines for program
 */
void setup()
{
#ifdef DEBUG
	Serial.begin(DEBUG_BAUDRATE);
	Serial.println("Starting Chicago!");
#endif

	/* setup bluetooth */
	bluetooth.begin(BT_BAUDRATE);

	/* setup wheels and drive system */
	int retCodes[4];
	retCodes[0] = wheel[0].setup(2, 34, false);
	retCodes[1] = wheel[1].setup(3, 35, false);
	retCodes[2] = wheel[2].setup(4, 36, false);
	retCodes[3] = wheel[3].setup(5, 37, false);
	drive.setWheels(wheel[0], wheel[1], wheel[2], wheel[3]);

#ifdef DEBUG
	Serial.println("Return codes for wheel initialization: ");
	Serial.println(retCodes[0]);
	Serial.println(retCodes[1]);
	Serial.println(retCodes[2]);
	Serial.println(retCodes[3]);
#endif

	drive.setPowerCartesian(0, 100, 0);
	drive.drive();
	delay(500);
	drive.setPowerCartesian(100, 0, 0);
	drive.drive();
	delay(500);
	drive.setPowerCartesian(0, 0, 0);
	drive.drive();
}

/**
 * Main loop routine for program
 */
void loop()
{
	if (bluetooth.available())
	{
		char inByte = bluetooth.read();

#ifdef DEBUG
		Serial.print("Received: ");
		Serial.print(inByte);
		Serial.print("\n");
#endif

		switch(inByte)
		{
		case('w'):
			drive.setPowerCartesian(0, 100, 0);
			break;
		case('a'):
			drive.setPowerCartesian(-100, 0, 0);
			break;
		case('s'):
			drive.setPowerCartesian(0, -100, 0);
			break;
		case('d'):
			drive.setPowerCartesian(100, 0, 0);
			break;
		case('j'):
			drive.setPowerCartesian(0, 0, 50);
			break;
		case('l'):
			drive.setPowerCartesian(0, 0, -50);
			break;
		case('k'):
			drive.setPowerCartesian(0, 0, 0);
			break;
		}
	}
	else

	drive.drive();
}

/**
 * Compiler main
 *
 * @return	termination return code
 */
int main(void) {
	init();
	setup();

	while(true) {
		loop();
	}

	return 0;
}


