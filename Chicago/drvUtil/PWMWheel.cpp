/**
 * Chicago - Project Broadway
 *
 * Wheel.cpp
 * Purpose: Implementations for the PWMWheel class
 *
 * @author George Xian
 * @version 0.1	20-02-2014
 */

#include "PWMWheel.hpp"

#include <Arduino.h>


int PWMWheel::setup(int powerPinNo, int directionPinNo, bool reverse)
{
	int retCode = 0x00;
#ifdef __AVR_ATmega2560__
	if (powerPinNo < 2 || powerPinNo > 13)
	{
		// no PWM pin case
		retCode |= (1<<0);
	}

	if (directionPinNo > 53)
	{
		// exceed maximum pin number case
		retCode |= (1<<1);
	}
#else
#warning "No pin configuration data for this target, chosen pin maybe invalid"
#endif

	powerPin = powerPinNo;
	directionPin = directionPinNo;
	reverseSet = reverse;

	prevSpd = 0;

	pinMode(powerPin, OUTPUT);
	pinMode(directionPin, OUTPUT);

	return retCode;
}

void PWMWheel::drive(int power)
{
	// TODO: replace with PWM timer
	analogWrite(powerPin, power);			// Specify power

	if (power >= 0)
	{
		/* move forwards */
		if (reverseSet)
		{
			digitalWrite(directionPin, LOW);
		}
		else
		{
			digitalWrite(directionPin, HIGH);
		}
	}
	else
	{
		/* move backwards */
		if (reverseSet)
		{
			digitalWrite(directionPin, HIGH);
		}
		else
		{
			digitalWrite(directionPin, LOW);
		}
	}

	prevSpd = power;
}

int PWMWheel::getPrevSpeed(void)
{
	return prevSpd;
}

bool PWMWheel::getReverseSetting(void)
{
	return reverseSet;
}

