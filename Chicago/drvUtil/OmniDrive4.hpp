/**
 * Chicago - Project Broadway
 *
 * OmniDrive4.hpp
 * Purpose: Declarations for the OmniDrive4 class
 *
 * @author George Xian
 * @version 0.1	20-02-2014
 */


#ifndef OMNIDRIVE4_HPP_
#define OMNIDRIVE4_HPP_

#include "PWMWheel.hpp"

#include <stdint.h>

/**
 * Abstracts a generic quad omni-wheel drive system.
 */
class OmniDrive4
{
	public:
		/**
		 * Constructor for this class. Links the wheels to the drive system.
		 *
		 * @param	FR			wheel instance for front right wheel
		 * @param	FL			wheel instance for front left wheel
		 * @param	RL			wheel instance for rear left wheel
		 * @param	RR			wheel instance for rear right wheel
		 */
		int setWheels(PWMWheel& FR, PWMWheel& FL, PWMWheel& RL, PWMWheel& RR);

		/**
		 * Set the intended direction to apply power to in
		 * cartesian co-ordinates.
		 *
		 * @param	x	 		power in the x direction, range [-256, 255]
		 * @param	y			power in the y direction, range [-256, 255]
		 * @param	yaw			anti-clockwise power in yaw from 0, range [-256, 255]
		 * @return 	return code indicating how if x and/or y was changed
		 */
		int setPowerCartesian(int x, int y, int yaw);

		/**
		 * Set the intended direction to apply power to in
		 * polar co-ordinates.
		 *
		 * @param	magnitude	polar magnitude of applied power, range [-128, 127]
		 * @param	angle		angle to apply power, range [0, 255] for [0, 180] degrees (anti-clockwise)
		 * @param	yaw			anti-clockwise power in yaw, range [-256, 255]
		 * @return	return code indicating how if magnitude and/or angle was changed
		 */
		int setPowerPolar(int magnitude, int angle, int yaw);

		/**
		 * Set the intended speed of each wheel manually
		 *
		 * @param	FR			FR wheel speed
		 * @param	FL			FL wheel speed
		 * @param	RL			RL wheel speed
		 * @param	RR			RR wheel speed
		 * @return  return code indicating whether speeds were changed
		 */
		int setPowerManual(int FR, int FL, int RL, int RR);

		/**
		 * Drives the motor PWM to the specified power parameters as
		 * set by setPowerCartesian(), setPowerPolar() or setPowerManual()
		 */
		void drive(void);

		/**
		 * Returns the previously set wheel speeds
		 *
		 * @param	wheel		which wheel to get speed
		 * @return	speed of the requested wheel
		 */
		int getPrevWheelSpd(uint8_t wheel);

	private:
		PWMWheel* wheel[4];		// FR, FL, RL, RR
		int newWheelSpd[4];
};


#endif /* OMNIDRIVE4_HPP_ */
