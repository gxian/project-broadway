/**
 * Chicago - Project Broadway
 *
 * OmniDrive4.cpp
 * Purpose: Implementations for the OmniDrive4 class
 *
 * @author George Xian
 * @version 0.1	20-02-2014
 */

#include "OmniDrive4.hpp"

#include <Arduino.h>


int OmniDrive4::setWheels(PWMWheel& FR, PWMWheel& FL, PWMWheel& RL, PWMWheel& RR)
{
	int retCode = 0x00;

	wheel[0] = &FR;
	wheel[1] = &FL;
	wheel[2] = &RL;
	wheel[3] = &RR;

	return retCode;
}

int OmniDrive4::setPowerCartesian(int x, int y, int yaw)
{
	int maxValue = abs(x) + abs(y) + abs(yaw);

	int oddQuadrant = y - x;	// quadrant I, III wheels
	int evenQuadrant = x + y;	// quadrant II, IV wheels

	/* apply the wheel speeds */
	newWheelSpd[0] = oddQuadrant + yaw; 	// FR
	newWheelSpd[1] = evenQuadrant - yaw;	// FL
	newWheelSpd[2] = oddQuadrant - yaw; 	// RL
	newWheelSpd[3] = evenQuadrant + yaw;	// RR

	int retCode;
	/* if necessary modify wheel speeds to preserve direction */
	if (maxValue > 255)
	{
		for (uint8_t i=0; i<4; i++)
		{
			newWheelSpd[i] = newWheelSpd[i] * 255 / maxValue;
		}
		retCode = 1;
	}
	else
	{
		retCode = 0;
	}

	return retCode;
}

int OmniDrive4::setPowerPolar(int magnitude, int angle, int yaw)
{
	return 1;
}

int OmniDrive4::setPowerManual(int FR, int FL, int RL, int RR)
{
	/* apply the wheel speeds */
	newWheelSpd[0] = FR;
	newWheelSpd[1] = FL;
	newWheelSpd[2] = RL;
	newWheelSpd[3] = RR;

	/* find max wheel speed */
	int currentMaxSpeed = 0;
	for (uint8_t i=0; i<4; i++)
	{
		int absMaxSpeed = abs(newWheelSpd[i]);
		if (absMaxSpeed > currentMaxSpeed)
		{
			currentMaxSpeed = absMaxSpeed;
		}
	}

	int retCode;
	/* if necessary modify wheel speeds to preserve direction */
	if (currentMaxSpeed > 255)
	{
		for (uint8_t i=0; i<4; i++)
		{
			newWheelSpd[i] = newWheelSpd[i] * 255 / currentMaxSpeed;
		}
		retCode = 1;
	}
	else
	{
		retCode = 0;
	}

	return retCode;
}

void OmniDrive4::drive(void)
{
	for (uint8_t i=0; i<4; i++)
	{
		wheel[i]->drive(newWheelSpd[i]);
	}
}

int OmniDrive4::getPrevWheelSpd(uint8_t wheel)
{
	return newWheelSpd[wheel];
}




