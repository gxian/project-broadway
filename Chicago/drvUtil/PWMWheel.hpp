/**
 * Chicago - Project Broadway
 *
 * Wheel.hpp
 * Purpose: Declarations for the Wheel class
 *
 * @author George Xian
 * @version 0.1	20-02-2014
 */

#ifndef PWMWHEEL_HPP_
#define PWMWHEEL_HPP_

/**
 * Abstracts a generic motor driven wheel. Where motor power
 * and direction are controlled via a PWM and a binary signal.
 */
class PWMWheel
{
	public:
		/**
		 * Set the GPIO pins used and co-ordinate system
		 *
		 * Returns a bitwise code, a bit that is 1 indicates that setup parameter
		 * is invalid. Return code key:
		 * LSB - invalid dirPinNo
		 * 2nd bit - invalid pwrPinNo
		 *
		 * @param	pwrPinNo	GPIO pin no. to supply PWM to motor, must be PWM capable
		 * @param	dirPinNo	GPIO pin no. to specify direction of motor
		 * @param	reverse		Whether hardware setting causes the direction signal to be reversed
		 * @return	return code indicating whether set pins are valid
		 */
		int setup(int pwrPinNo, int dirPinNo, bool reverse);

		/**
		 * Drives the wheel with the specified power and direction
		 *
		 * @param	power		PWM duty to give to the motor, negative for reverse
		 */
		void drive(int power);

		/**
		 * Get previously set speed
		 *
		 * @return	previously set speed
		 */
		int getPrevSpeed(void);

		/**
		 * Get the reverse setting
		 *
		 * @return	reverse setting
		 */
		bool getReverseSetting(void);

	private:
		int powerPin;
		int directionPin;
		bool reverseSet;

		int prevSpd;
};


#endif /* PWMWHEEL_HPP_ */
