/**
 * Chicago - Project Broadway
 *
 * main.hpp
 * Purpose: Parent routine handler for basic bluetooth motor controller
 *
 * @author George Xian
 * @version 0.1	20-02-2014
 */

int main(void);
