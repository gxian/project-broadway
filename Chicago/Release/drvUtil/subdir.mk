################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../drvUtil/OmniDrive4.cpp \
../drvUtil/PWMWheel.cpp 

OBJS += \
./drvUtil/OmniDrive4.o \
./drvUtil/PWMWheel.o 

CPP_DEPS += \
./drvUtil/OmniDrive4.d \
./drvUtil/PWMWheel.d 


# Each subdirectory must supply rules for building sources it contributes
drvUtil/%.o: ../drvUtil/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: AVR C++ Compiler'
	avr-g++ -I"C:\Users\Ultima1\Dev\Arduino\Chicago" -I"C:\Users\Ultima1\Dev\Arduino\ArduinoCore" -I"C:\Dev\Arduino\hardware\arduino\cores\arduino" -I"C:\Dev\Arduino\hardware\arduino\variants\mega" -Wall -Os -fpack-struct -fshort-enums -ffunction-sections -fdata-sections -funsigned-char -funsigned-bitfields -fno-exceptions -mmcu=atmega2560 -DF_CPU=16000000UL -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


